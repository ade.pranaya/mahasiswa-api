<?php

require APPPATH . '/libraries/REST_Controller.php';

class Mahasiswa extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Mahasiswa_model", "mahasiswa");
    }

    public function index_get()
    {
        // ketikan source code yang ada di modul
        $mahasiswa = $this->mahasiswa->getData()->result();
        $data = new stdClass();
        $data->mahasiswa = $mahasiswa;
        $this->response($data);
    }

    public function index_put()
    {
        // ketikan source code yang ada di modul
    }

    public function index_post()
    {
        // ketikan source code yang ada di modul
    }

    public function index_delete()
    {
       // ketikan source code yang ada di modul
    }
}